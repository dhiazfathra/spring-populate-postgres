
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author wahyu
 */
public class Gei {

    Config config = ConfigFactory.parseFile(new File("db.conf"));

    private final String url = config.getString("url");
    private final String user = config.getString("user");
    private final String password = config.getString("password");
    private final Integer totalIterationPerThread = config.getInt("totalIterationPerThread");
    private final Long sleepPerExecution = config.getLong("sleepPerExecution");
    private final Integer totalThreading = config.getInt("totalThreading");

    public Connection connect() {
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url, user, password);
            System.out.println("Connected to the PostgreSQL server successfully.");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return conn;
    }

    public void fireInsertQuery(Connection con)
    {
        String[] yangBisaDimasukinKeMeki = {"titit", "timun", "terong",
            "pisang","jari","tangan","dildo","senter","paralon","dst"};
        Statement stmnt = null;
        try{
            stmnt= con.createStatement();
        }catch (SQLException sqlEx){System.out.println(sqlEx.getMessage());}
        String sql; 
        for(int i=0; i<yangBisaDimasukinKeMeki.length;i++)
        {    
            sql = "INSERT INTO public.meki(barang) "
                    + "VALUES ('"+yangBisaDimasukinKeMeki[i]+"');";
            try{
                stmnt.executeUpdate(sql);
                fireSelectAllQuery(con);
                Thread.sleep(sleepPerExecution);
            }catch( SQLException sqlEx){System.out.println(sqlEx.getMessage());}
            catch( InterruptedException intExp){System.out.println(intExp.getMessage());}
        }
    }
    public void fireSelectAllQuery(Connection con)
    {
        Statement stmnt = null;
        String something;
        try{
            stmnt= con.createStatement();
        }catch (SQLException sqlEx){System.out.println(sqlEx.getMessage());}
        String sql = "select * from meki;";
        try{
            ResultSet rs = stmnt.executeQuery(sql);
            while (rs.next()){something="";};
        }catch( SQLException sqlEx){System.out.println(sqlEx.getMessage());}
    }
    
    public void createConnectionFireAndClose() {
        long timeStart = System.currentTimeMillis();
        Connection con = connect();
        for(int i=0; i<totalIterationPerThread;i++)
            fireInsertQuery(con);
        try{con.close();}catch(SQLException sqlEx){System.out.println(sqlEx.getMessage());}
        long timeEnd = System.currentTimeMillis();
        System.out.println("Execution: "+ (timeEnd - timeStart));
    }

    public static void main(String args[]) 
    {
        List<Thread> allThreads =new ArrayList<Thread>();
        Gei geiMain = new Gei();
        //create threads
        for(int i=0; i<geiMain.totalThreading;i++)
        {
            allThreads.add(new Thread() {
                public void run(){
                    Gei gei = new Gei();
                    gei.createConnectionFireAndClose();
                }
            });
        }

        //run threads
        for(int i=0; i<geiMain.totalThreading;i++)
            allThreads.get(i).start();
    }
}
